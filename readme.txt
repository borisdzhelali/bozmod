Place this folder under Dominions/Mods
Take the DM file and move it into the Dominions mod folder
To see what's in the mod in an easy way, download Sublime Text 3 and open the DM file, change the syntax highlighting to YAML format and you should see a nice nested structure.
if you have issues, PM Author